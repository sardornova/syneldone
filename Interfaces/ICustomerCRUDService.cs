using Microsoft.AspNetCore.WebUtilities;
using Synel.Models;
namespace Synel.Interfaces
{
    public interface ICustomerCRUDService
    {
        Task AddCustomerServiceAsync();
    }
}