using Microsoft.AspNetCore.WebUtilities;

namespace Synel.Interfaces
{
    public interface ICsvParserService
    {
        bool Parse(string file_to_parse);
    }   
}