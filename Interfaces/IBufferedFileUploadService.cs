using Microsoft.AspNetCore.WebUtilities;

namespace Synel.Interfaces
{
    public interface IBufferedFileUploadService
    {
        Task<bool> FileUpload(IFormFile file);
    }
}