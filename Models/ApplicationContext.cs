using Microsoft.EntityFrameworkCore;

namespace Synel.Models
{
    public class ApplicationContext: DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Files>()
            .Property(e => e.Id)
            .HasDefaultValueSql("gen_random_uuid()")
            .ValueGeneratedOnAdd();
            modelBuilder.Entity<Files>()
            .Property(e => e.CreatedAt)
            .HasDefaultValueSql("current_timestamp")
            .ValueGeneratedOnAdd();
            modelBuilder.Entity<UserModel>()
            .Property(e => e.Id)
            .HasDefaultValueSql("gen_random_uuid()")
            .ValueGeneratedOnAdd();
        }
        public DbSet<Files> files {get; set;}
        public DbSet<UserModel> user {get; set;} 
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options):base(options)
        {
            Database.EnsureCreated();
        } 


    }
}