using System;

using System.ComponentModel.DataAnnotations.Schema;
namespace Synel.Models
{
    public class UserModel
    {
        public Guid Id {get; set;}
        public string Payroll_Number {get; set;} = "";
        public string Forenames {get; set;} = "";
        public string Surname {get; set;} = "";
        [Column(TypeName="timestamp")]
        public DateTime Date_of_Birth {get; set;}
        public string Telephone {get; set;} = "";
        public string Mobile {get; set;} = "";
        public string Address {get; set;} = "";
        public string Address_2 {get; set;} = "";
        public string Postcode {get; set;} = "";
        public string EMail_Home {get; set;} = "";
        [Column(TypeName="timestamp")]
        public DateTime Start_Date {get; set;}
    }
}