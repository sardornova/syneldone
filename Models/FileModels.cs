using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel.DataAnnotations;

namespace Synel.Models
{
    public class Files
    {
        public Guid Id {get; set;}
        public string? Name {get; set;}
        public string? FilePath {get; set;}
        [Column(TypeName = "timestamp")]
        public DateTime CreatedAt {get; set;}
        [Column(TypeName = "timestamp")]
        public DateTime UpdatedAt {get; set;}
    }
}