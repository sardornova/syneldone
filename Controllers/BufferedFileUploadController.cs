using Microsoft.AspNetCore.Mvc;
using Synel.Interfaces;

namespace Synel.Controllers
{   
    public class BufferedFileUploadController : Controller
    {
        readonly IBufferedFileUploadService  _bufferedFileUploadService;

        public BufferedFileUploadController(IBufferedFileUploadService bufferedFileUploadService)
        {
            _bufferedFileUploadService = bufferedFileUploadService;
        }

        public IActionResult Index()
        {
           return View(); 
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            try
            {
                if(await _bufferedFileUploadService.FileUpload(file))
                {
                    ViewBag.Message = "File Upload Successfull!";
                    Task.Delay(5000);
                    return RedirectToAction("Index", "UserView");
                }
                else
                {
                    ViewBag.Message = "File Upload Failed in try!";
                }

            }
            catch(Exception ex)
            {
                ViewBag.Message = $"File Upload Failed {ex} ";
            }
            return View();
        }
    }
}