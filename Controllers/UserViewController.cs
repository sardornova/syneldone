using System.Collections.Immutable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Synel.Models;


namespace Synel.Controllers
{
    public class UserViewController : Controller
    {
        ApplicationContext _db;
        public readonly ILogger<UserViewController> _logger;

        public UserViewController(ApplicationContext db, ILogger<UserViewController> logger)
        {
            _db = db;
            _logger = logger;
        }


        public async Task<IActionResult> Index()
        {   
            
            return View(await _db.user.ToListAsync());
        }

    }
}