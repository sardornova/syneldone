using System.Data.Common;
using Microsoft.AspNetCore.WebUtilities;
using Synel.Interfaces;
using Synel.Models;
using Synel.Services;
using CsvHelper;
using System.Globalization;
using System.Text;

namespace Synel.Services
{
    public class BufferedFileUploadService : IBufferedFileUploadService
    {   
        ApplicationContext _db;
        readonly ICustomerCRUDService _customerCRUDService;

        public BufferedFileUploadService(ApplicationContext db, ICustomerCRUDService customerCRUDService)
        {
            _db = db;
            _customerCRUDService = customerCRUDService;
        }
        public async Task<bool> FileUpload(IFormFile file)
        {
            string path = "";

            try
            {
            
                if (file.Length > 0 )
                {
                    path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "UploadedFiles"));
                    if(!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    string file_to_parse = $"{DateTime.Now.ToString("yyyy_MM_dd_")}{file.FileName}";
                    using(var fileStream = new FileStream(Path.Combine(path,file_to_parse), FileMode.Create))
                    {

                        Files file_location = new Files {Name = file.FileName, FilePath = Path.Combine(Environment.CurrentDirectory, file.FileName), CreatedAt =DateTime.Now, UpdatedAt = DateTime.Now };
                        await file.CopyToAsync(fileStream);
                        _db.files.Add(file_location);
                        await _db.SaveChangesAsync();
                        
                        
                    }
                    await _customerCRUDService.AddCustomerServiceAsync();
                    return CsvEditorService.Handler(Path.Combine(path,file_to_parse));
                    
                
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("File copy Failed", ex);
            }

        }
    }
}