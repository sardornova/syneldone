using System.Data.Common;
using System.Globalization;
using CsvHelper;
using Synel.Models;
using Synel.HelperModel;
using Microsoft.AspNetCore.WebUtilities;


namespace Synel.Services
{
    public class CsvParserService
    {  
        public static List<UserModel> Parse(string filePath)
        {   
            List<UserModel> return_data = new List<UserModel>();
            try
            {
            //using(ApplicationContext db = new ApplicationContext())
            using(var rd = new StreamReader(filePath))
            using(var csv_reader = new CsvReader(rd, CultureInfo.InvariantCulture))
            {
                var records = csv_reader.GetRecords<User>().ToList();
                foreach (var record in records)
                {
                    DateTime birthDate;
                    DateTime startDate;
                    DateTime.TryParseExact(record.Date_of_Birth, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate);
                    DateTime.TryParseExact(record.Start_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                    var records_to_send = new UserModel{
                        Payroll_Number = record.Payroll_Number, 
                        Forenames = record.Forenames, 
                        Surname = record.Surname,
                        Date_of_Birth = birthDate,
                        Telephone = record.Telephone,
                        Mobile = record.Mobile,
                        Address = record.Address,
                        Address_2 = record.Address_2,
                        Postcode = record.Postcode,
                        EMail_Home = record.EMail_Home,
                        Start_Date = startDate
                        };
                
                    return_data.Add(records_to_send);
                }
                return return_data;
            }
                throw new Exception("Smth went wrong and this is catch"); 
            }
            
            catch (Exception ex)
            {
                throw new Exception("Smth went wrong and this is catch", ex);
                
            }
        
        }
    }
}