using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using CsvHelper;
using Synel.Models;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
using System.Data.Common;
using System.Data;
namespace Synel.Services
{
  public class CsvEditorService
  {

    public static bool Handler(string file_to_parse)
    {
            using(var reader = new StreamReader(file_to_parse))
            using(var source_csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                string line;
                StringBuilder output = new StringBuilder();
                while((line = reader.ReadLine()) != null)
                {   
                    string original_header = line;
                    string result_header = original_header.Replace("Personnel_Records.","");
                    output.AppendLine(string.Join(",",result_header));
                    
                }

                try
                {
                    using(StreamWriter writer = new StreamWriter("dataset.csv", false))
                    {
                        writer.Write(output.ToString());  
                    }
                    CsvParserService.Parse("dataset.csv");
                    return true;
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("",ex);   
                }
            }
    }
  }
}