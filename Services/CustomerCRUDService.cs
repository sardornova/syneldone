using Synel.Interfaces;
using Synel.Models;

namespace Synel.Services
{
    public class CustomerCRUDService : ICustomerCRUDService
    {
        ApplicationContext _db;
        public CustomerCRUDService(ApplicationContext db)
        {
            _db = db;
        }
        public async Task AddCustomerServiceAsync()
        {       try
            {
                List<UserModel> customers = CsvParserService.Parse("dataset.csv");
                foreach(var customer in customers )
                {
                    _db.user.Add(customer);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Smth went wrong on CRUD", ex);
            }

            
        }
    }
}